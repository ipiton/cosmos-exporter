package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
)

type APIStatus struct {
	Result struct {
		SyncInfo struct {
			LatestBlockHeight string `json:"latest_block_height"`
			LatestBlockTime string `json:"latest_block_time"`
		} `json:"sync_info"`
	} `json:"result"`
}

type netInfoResponse struct {
	Result struct {
		NPeers string `json:"n_peers"`
	} `json:"result"`
}

var (
	blockHeight = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "block_height",
		Help: "Latest block height from the API",
	})
	blockTime = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "block_time",
		Help: "Latest block time from the API",
	})
	blockTimeDiff = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "block_time_diff",
		Help: "Time difference between now and latest block time from the API",
	})
	nPeers = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "blockchain_n_peers",
		Help: "Number of peers connected to the blockchain node",
	})
)

func init() {
	// Register the metrics with Prometheus
	prometheus.MustRegister(blockHeight)
	prometheus.MustRegister(blockTime)
	prometheus.MustRegister(blockTimeDiff)
	prometheus.MustRegister(nPeers)

}
func getAPIStatus(url string) {
  start := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		log.Errorf("Error getting APIStatus response: %v", err)
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("Error reading APIStatus response: %v", err)
		return
	}

	// log.Infof("APIStatus response: %s", body)

	var apiStatus APIStatus
	err = json.Unmarshal(body, &apiStatus)
	if err != nil {
		log.Errorf("Error unmarshalling JSON APIStatus: %v", err)
		return
	}

	blockHeightValue, err := strconv.ParseFloat(apiStatus.Result.SyncInfo.LatestBlockHeight,64)
	if err != nil {
		log.Errorf("Error parsing latest block height: %v", err)
		return
	}

	blockHeight.Set(blockHeightValue)
	log.Infof("Latest block height: %f", blockHeightValue)

	blockTimeValue, err := time.Parse(time.RFC3339, apiStatus.Result.SyncInfo.LatestBlockTime)
	if err != nil {
		log.Errorf("Error parsing latest block time: %v", err)
		return
	}

	blockTime.Set(float64(blockTimeValue.Unix()))
	log.Infof("Latest block time: %s", blockTimeValue)

	blockTimeDiffValue := time.Since(blockTimeValue).Seconds()
	blockTimeDiff.Set(blockTimeDiffValue)
	log.Infof("Time difference: %f", blockTimeDiffValue)

	log.Infof("Finished processing APIStatus response in %v", time.Since(start))

}

func getNetInfoMetrics(apiURL string) {
	start := time.Now()

	// Get the API response
	res, err := http.Get(apiURL)
	if err != nil {
		log.Errorf("Error getting NetInfo response: %v", err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Errorf("Error reading NetInfo response: %v", err)
		return
	}

	// log.Infof("NetInfo response: %s", body)

	var netInfo netInfoResponse
	err = json.Unmarshal(body, &netInfo)
	if err != nil {
		log.Errorf("Error unmarshalling JSON NetInfo: %v", err)
		return
	}

	nPeersValue, err := strconv.ParseFloat(netInfo.Result.NPeers,64)
	if err != nil {
		log.Errorf("Error parsing latest block height: %v", err)
		return
	}

  // Set the n_peers metric value
	nPeers.Set(nPeersValue)
	log.Infof("Number of peers: %f", nPeersValue)

	log.Infof("Finished processing NetInfo response in %v", time.Since(start))
}


func main() {
	// Start a go routine to periodically fetch API metrics
	ticker := time.NewTicker(15 * time.Second)
	go func() {
		for range ticker.C  {
			getAPIStatus("http://127.0.0.1:26657/status")
			getNetInfoMetrics("http://127.0.0.1:26657/net_info")
		}
	}()

	// Serve Prometheus metrics
	http.Handle("/metrics", promhttp.Handler())
	log.Info("Starting Prometheus exporter server")
	log.Fatal(http.ListenAndServe(":9095", nil))
}
